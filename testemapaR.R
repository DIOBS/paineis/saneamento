library(sf)

#> Linking to GEOS 3.6.1, GDAL 2.1.3, PROJ 4.9.3
 
ccg <- st_read('arquivo1.kml')
#> Reading layer `OGRGeoJSON' from data source `https://opendata.arcgis.com/datasets/de108fe659f2430d9558c20fe172638f_2.kml' using driver `KML'
#> Simple feature collection with 207 features and 2 fields
#> geometry type:  MULTIPOLYGON
#> dimension:      XY
#> bbox:           xmin: -6.418524 ymin: 49.86474 xmax: 1.762942 ymax: 55.81107
#> epsg (SRID):    4326
#> proj4string:    +proj=longlat +datum=WGS84 +no_defs
ccg[1]
ccg$Name<-substring(ccg$Name,17) 

plot(ccg[1])

legend("topleft", legend = c(ccg$Name))
